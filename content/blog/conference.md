---
author: "Life Choices"
title: "A Conference"
date: "2023-05-09"
description: "We had it"
tags: ["conference"]
categories: ["conference"]
ShowToc: true 
TocOpen: true
---
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et convallis enim, porttitor auctor mauris.e

### It was amazing

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus et convallis enim, porttitor auctor mauris. Pellentesque eget leo efficitur, consequat mi in, euismod ex. Nullam efficitur, ligula ac facilisis posuere, elit ipsum interdum sapien, a malesuada ipsum magna sit amet ipsum. Donec sagittis ultricies tincidunt. Nullam vitae mi quam. Maecenas egestas, odio eget pellentesque tincidunt, lectus nisl fringilla quam, et sagittis odio diam a augue. Sed commodo velit ac vestibulum accumsan.

Aenean ultrices at ex eu cursus. Nam nec mi arcu. Maecenas interdum porttitor ex vel porttitor. Duis nec consequat justo, a fermentum ligula. Morbi vehicula interdum lectus sed finibus. Nam a orci mi. Vivamus consequat, risus in tempor fermentum, sapien ligula tempus lorem, ut ultrices leo nibh non mi. Duis molestie efficitur ullamcorper. Etiam ac dui mi. Cras vitae mi aliquet, blandit sapien et, iaculis nibh. Mauris lacinia vulputate orci in venenatis. Suspendisse vel massa quis nulla sagittis efficitur. Cras ac libero id leo rutrum euismod vel vel massa. Quisque tempor erat nulla, ut pulvinar lectus molestie et. Donec bibendum sed mauris sed vestibulum. Morbi ullamcorper erat nisl, quis viverra diam tempus non.

### More Stuff
Morbi sed aliquam metus, a ultrices ipsum. Phasellus varius, lectus ac ultricies tincidunt, risus metus feugiat lectus, at tempor nulla felis quis arcu. Mauris facilisis enim vitae diam tempus, vel vestibulum velit malesuada. Phasellus pretium dapibus lectus eget mattis. Integer urna tortor, aliquet at elementum id, gravida ut turpis. Pellentesque vitae faucibus leo, auctor mollis risus. Maecenas et finibus magna, eu vehicula augue. Etiam rutrum ullamcorper lectus tristique fringilla. Nunc sed mi id arcu blandit rhoncus. Nam fringilla vel nibh eget molestie. Aliquam erat volutpat.

Aliquam quis semper magna. Suspendisse vehicula elit non odio iaculis, ut volutpat velit scelerisque. Integer dictum nec odio id dapibus. Praesent nec tincidunt lectus, a semper nulla. Sed faucibus augue eu auctor laoreet. Nunc est odio, suscipit a cursus non, tristique eget mi. Duis in lorem et velit congue feugiat. Donec dignissim eget ipsum sit amet dictum. Aliquam non orci ex. Proin eu porttitor nibh. Pellentesque convallis diam id nisl commodo, vel congue erat luctus. Nulla facilisi. Quisque molestie urna porttitor, pretium erat vel, aliquam tellus.

Suspendisse vitae eleifend mi. Curabitur id finibus mi, at porta enim. Cras non turpis molestie, tempus orci congue, vehicula metus. Etiam interdum leo id porttitor placerat. Ut ultricies augue orci, nec pharetra risus tempor nec. Nullam at urna vitae orci molestie ullamcorper. Vestibulum iaculis euismod neque sit amet tempor. Mauris neque turpis, condimentum vitae commodo eget, bibendum eget ligula. Aliquam aliquet faucibus tortor a hendrerit. Donec ac nulla elementum, rutrum ipsum elementum, eleifend sapien. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris posuere magna sed sagittis vestibulum. Vivamus et nibh eros. Nam blandit quam quam, non cursus massa aliquam id.
