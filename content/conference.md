---
title: Conference
url: conference
---

### Info 

The Breaking Barriers conference is presented by Life Choices. This event is aimed to bring God's family together, for a time of worship, teaching, Q & A panels,  and so much more. We have something for all members of the church family.  Our kids  12 and under will simultaneously enjoy a full day of fun activities centered on loving one another. The desire is to inspire and encourage one another in practical ways to love each person we come in contact with in the vulnerable situations we all can find ourselves in.  

### Register

{{< button href="https://secure.fundeasy.com/ministrysync/event/?e=24169" title="Register Now" >}}
